let root = null;
const vscode = acquireVsCodeApi();
const margin = {
        top: 20,
        right: 120,
        bottom: 20,
        left: 120
    },
    width = 960 - margin.right - margin.left,
    height = 800 - margin.top - margin.bottom;

const nodeMargin = {
    top: 10,
    right: 10,
    bottom: 10,
    left: 10
};

const lineHeight = 8;
const fontSize = 12;
const dx = 20;
const dy = 360;
const xSpacing = 40;
const ySpacing = 10;
const i = 0,
    duration = 750,
    rectW = n => n.data._size[1] - xSpacing,
    rectH = n => n.data._size[0];

const tree = d3.flextree({
    nodeSize: n => n.data.size(!!n.children),
    spacing: ySpacing
});

const diagonal = d3
    .linkHorizontal()
    .x(d => d.y)
    .y(d => d.x);

function redraw() {
    svg.attr('transform', d3.event.transform);
}
const svg = d3
    .select('svg')
    .attr('width', '100%')
    .attr('height', '100%')
    .call(
        (zm = d3
            .zoom()
            .scaleExtent([0.1, 3])
            .on('zoom', redraw))
    )
    .append('g')
    .attr('transform', 'translate(' + 800 + ',' + 800 + ')');

const maxChars = 50;
function prettyCode(tokens) {
    let currentCharIndex = 1,
        currentLineIndex = tokens[0].data.sourceLocation.startLine,
        line = '',
        lines = [];

    for (const token of tokens) {
        const loc = token.data.sourceLocation;
        if (currentLineIndex < loc.startLine) {
            lines.push(line);
            line = '';
            currentCharIndex = 1;
        }

        while (currentLineIndex < loc.startLine) {
            currentLineIndex++;
            lines.push('');
        }

        if (currentCharIndex < loc.startCharacter) {
            line += ' '.repeat(loc.startCharacter - currentCharIndex);
        }

        line += token.data.shortName;
        currentCharIndex = loc.endCharacter;
    }
    if (line != '') lines.push(line);
    return lines;
}

const gLink = svg
    .append('g')
    .attr('fill', 'none')
    .attr('stroke', '#555')
    .attr('stroke-opacity', 0.4)
    .attr('stroke-width', 1.5);

const gNode = svg
    .append('g')
    .attr('cursor', 'pointer')
    .attr('pointer-events', 'all');

class NodeData {
    constructor(node) {
        this.id = node.id;
        this.shortName = node.shortName;
        this.fullName = node.fullName;
        this.type = node.type;
        this.sourceLocation = node.sourceLocation;
        this.children = node.children;
        this._leaves = node._leaves;
    }

    get lines() {
        if (this._lines) return this._lines;
        this._lines = prettyCode(this._leaves);
        return this._lines;
    }

    size(isInnerNode) {
        this._size = true ? [lineHeight + dx, dy] : [this.lines.length * lineHeight + dx, dy];
        return this._size;
    }
}
class TreeRenderer {
    constructor(svg) {
        this.svg = svg;
    }

    render(source) {
        const duration = d3.event && d3.event.altKey ? 2500 : 250;
        const nodes = root.descendants().reverse();
        const links = root.links();

        // Compute the new tree layout.
        tree(root);

        // With single root tree does not recalculate size :(
        if (!root.children) {
            root.data.size(false);
        }
        let left = root;
        let right = root;
        root.eachBefore(node => {
            if (node.x < left.x) left = node;
            if (node.x > right.x) right = node;
        });

        const height = right.x - left.x + margin.top + margin.bottom;

        const transition = this.svg
            .transition()
            .duration(duration)
            .attr('viewBox', [-margin.left, left.x - margin.top, width, height])
            .tween('resize', window.ResizeObserver ? null : () => () => this.svg.dispatch('toggle'));

        // Update the nodes…
        const node = gNode.selectAll('g').data(nodes, d => d.id);

        this._joinData(node, source, transition);

        // Update the links…
        const link = gLink.selectAll('path').data(links, d => d.target.id);
        link.join(
            enter =>
                enter.append('path').attr('d', d => {
                    const o = { x: source.x0, y: source.y0 };
                    return diagonal({ source: o, target: o });
                }),
            update => update,
            exit =>
                exit
                    .transition(transition)
                    .remove()
                    .attr('d', d => {
                        const o = { x: source.x, y: source.y };
                        return diagonal({ source: o, target: o });
                    })
        )
            .transition(transition)
            .attr('d', d => {
                const source = { ...d.source, y: d.source.y + dy / 2 - 20 };
                const target = { ...d.target, y: d.target.y - dy / 2 + 20 };
                return diagonal({ source, target });
            })
            .attr('stroke', d => (d.source.data.onPath && d.target.data.onPath ? '#ff5733' : '#555'))
            .attr('stroke-opacity', d => d.source.data.onPath && d.target.data.onPath && 1)
            .attr('marker-start', 'url(#triangle)');

        // Stash the old positions for transition.
        root.eachBefore(d => {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    _joinData(selection, source, transition) {
        selection
            .join(
                enter => {
                    const group = enter
                        .append('g')
                        .attr('transform', d => `translate(${source.y0},${source.x0})`)
                        .attr('fill-opacity', 0)
                        .attr('stroke-opacity', 0)
                        .on('click', d => this._onClick(d));
                    group.append('rect');
                    group.filter(d => d.children).call(this._astNodeText);
                    group.filter(d => !d.children).call(this._leafNodeText);
                    return group;
                },
                update => {
                    update.filter(d => d.selected && d.children).call(this._astNodeText);
                    update.filter(d => d.selected && !d.children).call(this._leafNodeText);
                    return update;
                },
                exit =>
                    exit
                        .transition(transition)
                        .remove()
                        .attr('transform', d => `translate(${source.y},${source.x})`)
                        .attr('fill-opacity', 0)
                        .attr('stroke-opacity', 0)
            )
            .call(s => s.transition(transition).attr('transform', d => `translate(${d.y},${d.x})`))
            .call(this._drawRect)
            .attr('fill-opacity', 1)
            .attr('stroke-opacity', 1);
    }

    _onClick(d) {
        if (d3.event.ctrlKey) {
            const recurse = (n, f) => {
                n.children = f ? null : n._children;
                n._children && n._children.map(x => recurse(x, f));
            };
            recurse(d, d.children);
        } else {
            d.children = d.children ? null : d._children;
            console.log(d.data);
        }
        d.selected = true;
        vscode.postMessage({ command: 'selected', data: { id: d.data.id } });
        this.render(d);
    }
    _astNodeText(selection) {
        selection
            .selectAll('text')
            .data(d => [d.data.shortName])
            .join('text')
            .attr('text-anchor', d => 'middle')
            .attr('font-size', fontSize)
            .attr('font-family', 'monospaced')
            .text(d => d)
            .attr('y', 3)
            .attr('x', 0);
    }

    _leafNodeText(selection) {
        selection
            .selectAll('text')
            .data(d => d.data.lines.slice(0, 1).map(x => ({ text: x, data: d.data })))
            .join('text')
            .attr('text-anchor', d => 'start')
            .attr('font-size', fontSize)
            .attr('style', (d, i) => i && 'white-space:pre')
            .attr('font-family', 'monospaced')
            .text(d => d.text)
            .attr('y', (d, i) => -rectH(d) / 2 + nodeMargin.top + (i + 1) * 6)
            .attr('x', d => -rectW(d) / 2 + nodeMargin.left);
    }
    _drawRect(selection) {
        selection
            .selectAll('rect')
            .attr('width', rectW)
            .attr('height', rectH)
            .attr('y', d => -rectH(d) / 2)
            .attr('x', d => -rectW(d) / 2)
            .attr('rx', d => (d.children ? 5 : 0))
            .attr('stroke', 'black')
            .attr('stroke-width', 1)
            .style('fill', function(d) {
                if (d.data.onPath) return '#ff5733';
                if (d.data.type == 'usage') return 'IndianRed';
                if (d.data.type == 'definition') return 'MediumSeaGreen';
                return d._children ? 'lightsteelblue' : '#fff';
            });
    }
}

const map = new Map();
svg.append('svg:defs')
    .append('svg:marker')
    .attr('id', 'triangle')
    .attr('refX', 6)
    .attr('refY', 6)
    .attr('markerWidth', 30)
    .attr('markerHeight', 30)
    .attr('markerUnits', 'userSpaceOnUse')
    .attr('orient', 'auto')
    .append('path')
    .attr('d', 'M 0 0 12 6 0 12 3 6')
    .style('fill', 'black');
const renderer = new TreeRenderer(svg);
const path = [42, 41, 38, 37, 33, 0, 1, 3];

// console.log('hello');
function display(data) {
    root = tree.hierarchy(data);
    root.x0 = dy / 2;
    root.y0 = 0;
    root.descendants().forEach((d, i) => {
        d.id = i;
        d._children = d.children;
        d.data = new NodeData({ ...d.data, _leaves: d.leaves() });
        map[d.data.id] = d;

        d.data.onPath = path.findIndex(x => x == d.data.id) > -1;
        console.log('X', d.data.id, d.data.onPath);
        if (d.depth) d.children = null;
    });
    renderer.render(root);
}

window.addEventListener('message', event => {
    const message = event.data;
    switch (message.command) {
        case 'display':
            console.log('display');
            display(message.data);
            break;
    }
});
// d3.json('data2.json').then(data => {
//     root = tree.hierarchy(data);
//     root.x0 = dy / 2;
//     root.y0 = 0;
//     root.eachAfter((d, i) => {
//         d.id = i;
//         d._children = d.children;

//         // d.data.size = calculateNodeSize(d);
//         d.data = new NodeData({ ...d.data, _leaves: d.leaves() });
//         map[d.data.id] = d;
//         d.data.onPath = path.findIndex(x => x == d.data.id) > -1;
//         if (d.depth) d.children = null;
//     });
//     renderer.render(root);
// });
vscode.postMessage({ command: 'ready' });
