import * as vscode from 'vscode';

const decorType = (color: string, border: string) => {
    return vscode.window.createTextEditorDecorationType({
        backgroundColor: color,
        border: `solid 1px ${border}`
    });
};
export class ColorPicker {
    private static colors = [
        ['#004d2d', '#0f5c3e', '#1e6b4f', '#2d7b61', '#3c8b73', '#4d9b86', '#61ab9a', '#78baaf'],
        ['#001088', '#232193', '#37319e', '#4841a8', '#5851b2', '#6960bc', '#7970c4', '#8c80cb']
    ];
    private static revColors = [
        ['#9fc6c6', '#78baaf', '#61ab9a', '#4d9b86', '#3c8b73', '#2d7b61', '#1e6b4f', '#0f5c3e'],
        ['#bf88b3', '#8c80cb', '#7970c4', '#6960bc', '#5851b2', '#4841a8', '#37319e', '#232193']
    ];
    private static n = ColorPicker.colors[0].length;
    private static decors = [
        [
            ColorPicker.colors[0].map(x => decorType(x, ColorPicker.colors[0][0])),
            ColorPicker.colors[1].map(x => decorType(x, ColorPicker.colors[1][0]))
        ],
        [
            ColorPicker.revColors[0].map(x => decorType(x, ColorPicker.revColors[0][ColorPicker.n - 1])),
            ColorPicker.revColors[1].map(x => decorType(x, ColorPicker.revColors[1][ColorPicker.n - 1]))
        ]
    ];

    public static pick(p: number, i: number) {
        return ColorPicker.decors[Math.floor(i / ColorPicker.n) % 2][p][i % ColorPicker.n];
    }

    public static reset(editor: vscode.TextEditor) {
        const decors = ColorPicker.decors;
        for (let i = 0; i < decors.length; i++)
            for (let j = 0; j < decors[0].length; j++)
                for (let k = 0; k < decors[0][0].length; k++) editor.setDecorations(decors[i][j][k], []);
    }
}
