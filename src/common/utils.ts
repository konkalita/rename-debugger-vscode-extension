import * as path from 'path';
import * as vscode from 'vscode';

export function getSpecification() {
    return path.join(vscode.workspace.rootPath || '', vscode.workspace
        .getConfiguration()
        .get('rename.specification.filename') as string);
}

export function getPaths(document: vscode.TextDocument) {
    const paths = { specificationFile: getSpecification(), sourceFile: document.fileName.split('.git')[0] };
    console.log(paths);
    return paths;
}
