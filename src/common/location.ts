import * as path from 'path';

export interface Location {
    source: path.ParsedPath;
    startLine: number;
    endLine: number;
    startCharacter: number;
    endCharacter: number;
}
