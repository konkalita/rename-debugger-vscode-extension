import * as vscode from 'vscode';
import { toRange, AstNode } from '../ast/ast.node';
import { ColorPicker } from '../common/color.picker';
import { getUseDefPath } from '../api';

function cutRangeDesc(r1: vscode.Range, r2: vscode.Range) {
    if (r1.end.line > r2.start.line) {
        return new vscode.Range(r1.start, r2.start);
    }
    if (r1.end.line === r2.start.line && r2.end.character > r1.start.character) {
        return new vscode.Range(r1.start, r2.start);
    }
    return r1;
}

function cutRangeAsc(r1: vscode.Range, r2: vscode.Range) {
    if (r1.start.line > r2.end.line) {
        return new vscode.Range(r2.end, r1.end);
    }
    if (r1.start.line === r2.end.line && r2.start.character < r1.start.character) {
        return new vscode.Range(r2.end, r1.end);
    }
    return r1;
}

export function displaySegment(editor: vscode.TextEditor, nodes: AstNode[]) {
    ColorPicker.reset(editor);
    let prevRange = null;
    let min = nodes[0].id,
        minId = 0;
    const decors = [];
    for (let i = 0; i < nodes.length; i++) {
        decors.push(ColorPicker.pick(1, i));
        if (min > nodes[i].id) {
            min = nodes[i].id;
            minId = i;
        }
    }
    if (minId !== 0) {
        let prevRange = toRange(nodes[0].sourceLocation);
        editor.setDecorations(decors[0], [{ range: prevRange, hoverMessage: nodes[0].shortName }]);
        console.log('up');
        for (let i = 1; i < minId; i++) {
            const { sourceLocation, shortName } = nodes[i];
            const range = toRange(sourceLocation);
            const decorRange = cutRangeDesc(range, prevRange);

            prevRange = decorRange;
            editor.setDecorations(decors[i], [{ range: decorRange, hoverMessage: shortName }]);
        }
    }

    const n = nodes.length - 1;
    if (minId !== n) {
        prevRange = toRange(nodes[n].sourceLocation);
        editor.setDecorations(decors[n], [{ range: prevRange, hoverMessage: nodes[n].shortName }]);
        console.log('down');
        for (let i = n - 1; i > minId; i--) {
            const { sourceLocation, shortName } = nodes[i];
            const range = toRange(sourceLocation);
            const decorRange = cutRangeAsc(range, prevRange);
            prevRange = decorRange;
            editor.setDecorations(decors[i], [{ range: decorRange, hoverMessage: shortName }]);
        }
    }

    if (minId !== 0 && minId !== n) {
        const prev = toRange(nodes[minId + 1].sourceLocation),
            next = toRange(nodes[minId - 1].sourceLocation);
        console.log('mid');
        const ranges = [];
        for (let i = prev.end.line; i <= next.start.line; i++) {
            ranges.push({
                range: new vscode.Range(i, prev.start.character, i, prev.start.character + 1),
                hoverMessage: nodes[minId].shortName
            });
        }
        editor.setDecorations(decors[minId], ranges);
    }
}

export async function useDefPath(body: any) {
    console.log('highlight');
    const data = await getUseDefPath(body);
    console.log(data);
    const editor = vscode.window.activeTextEditor;

    if (data !== undefined && editor !== undefined) {
        let p = -1;
        for (let { name, nodes } of data) {
            displaySegment(editor, nodes);
        }
    }
}
