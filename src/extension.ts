// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as path from 'path';

import * as cp from 'child_process';
import { VariableTreeProvider, VariableNode } from './tree-views/variable.tree.provider';
import { CallTreeProvider, CallNode, EquationNode } from './tree-views/call.tree.provider';
import { AstNavigator } from './ast/ast.navigation';
import { AstNode, SourceLocation, toRange } from './ast/ast.node';
import { RuleCodeLensProvider } from './rule.code.lens.provider';
import { useDefPath, displaySegment } from './commands/use.def.path';
import { getSpecification, getPaths } from './common/utils';
import {
    getEquations,
    getCallTree,
    getAst,
    Paths,
    getUsages,
    getUseDefPath,
    getDefinitions,
    getVisibilityScope,
    notifyChange
} from './api';
import { ColorPicker } from './common/color.picker';
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

const AstHighlightDecorationType = vscode.window.createTextEditorDecorationType({
    backgroundColor: 'blue',
    after: {
        fontWeight: 'normal',
        fontStyle: 'italic',
        textDecoration: `none;`,
        backgroundColor: 'black',
        border: '1px solid white'
    },
    rangeBehavior: vscode.DecorationRangeBehavior.ClosedOpen
});

function updateTreeViews(request: { paths: Paths; nodeId: number }) {
    getEquations(request).then(data => {
        vscode.window.registerTreeDataProvider('variables', new VariableTreeProvider(data));
    });

    getCallTree(request).then(data => {
        vscode.window.registerTreeDataProvider('calls', new CallTreeProvider(data));
    });
}

function displayNode(editor: vscode.TextEditor, node: AstNode) {
    console.log(editor.document.uri.fsPath);
    const range = toRange(node.sourceLocation);
    editor.revealRange(range);
    const decor = {
        renderOptions: {
            after: {
                contentText: ` ${node.shortName} `
            }
        },
        range: range
    };
    editor.setDecorations(AstHighlightDecorationType, [decor as vscode.DecorationOptions]);
    const specificationFile = getSpecification();
    const request = {
        nodeId: node.id,
        paths: { specificationFile: specificationFile, sourceFile: editor.document.uri.fsPath }
    };

    updateTreeViews(request);
}
function goToNode(direction: 'up' | 'down' | 'left' | 'right') {
    const editor = vscode.window.activeTextEditor;
    if (editor !== undefined) {
        const navigator = navigators.get(editor);
        const node = navigator && navigator.goTo(direction);
        const _ = node && displayNode(editor, node);
    }
}
class RuleCallerProvider implements vscode.DefinitionProvider {
    provideDefinition(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken) {
        console.log('def provider');
        const calls = (token as unknown) as AstNode[];
        return calls.map(
            call => new vscode.Location(vscode.Uri.parse(call.sourceLocation.source), toRange(call.sourceLocation))
        );
    }
}

async function getNavigator(): Promise<any> {
    const editor = vscode.window.activeTextEditor;
    if (editor !== undefined) {
        const navigator = navigators.get(editor);
        if (navigator !== undefined) {
            return Promise.resolve([editor, navigator]);
        }
    }
    return Promise.reject();
}

function displayScope(editor: vscode.TextEditor, nodes: AstNode[]) {
    ColorPicker.reset(editor);
    editor.setDecorations(ColorPicker.pick(1, 0), nodes.map(x => toRange(x.sourceLocation)));
}

const navigators: Map<vscode.TextEditor, AstNavigator> = new Map();
export function activate(context: vscode.ExtensionContext) {
    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "renamedebugger" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    let panel: vscode.WebviewPanel | undefined = undefined;

    let docSelector = {
        language: 'rename',
        scheme: 'file'
    };
    context.subscriptions.push(vscode.languages.registerDefinitionProvider(docSelector, new RuleCallerProvider()));
    // Register our CodeLens provider
    context.subscriptions.push(vscode.languages.registerCodeLensProvider(docSelector, new RuleCodeLensProvider()));
    context.subscriptions.push(
        vscode.commands.registerCommand('extension.goToLocation', arg => {
            if ('sourceLocation' in arg) {
                (vscode.window.activeTextEditor as vscode.TextEditor).revealRange(toRange(arg.sourceLocation));
            }
        })
    );
    context.subscriptions.push(
        vscode.commands.registerCommand('extension.goToNode', () => {
            const editor = vscode.window.activeTextEditor;
            if (editor !== undefined) {
                const navigator = navigators.get(editor);
                const node = navigator && navigator.goToPosition(editor.selection.active);
                // tslint:disable-next-line: no-unused-expression
                node && displayNode(editor, node);
            }
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('extension.visibilityScope', async () => {
            const [editor, navigator] = await getNavigator();

            const paths = { sourceFile: editor.document.uri.fsPath, specificationFile: getSpecification() };
            const usages = await getDefinitions({
                paths,
                nodeId: navigator.currentNode.id
            });
            const result = await vscode.window.showQuickPick(
                usages.map(u => `${u.id}: ${u.name}: ${u.stackTrace[0]}`),
                {
                    placeHolder: 'Choose definition'
                }
            );
            const id = (result && +result.split(':')[0]) as number;
            console.log(id);
            const path = await getVisibilityScope({ paths, definitionId: id });
            console.log(path);
            editor.setDecorations(AstHighlightDecorationType, []);
            await vscode.window.showQuickPick(Object.keys(path), {
                placeHolder: 'Choose segment',
                onDidSelectItem: (item: string) => displayScope(editor, path[item])
            });
        })
    );
    context.subscriptions.push(
        vscode.commands.registerCommand('extension.resetDecorations', async () => {
            console.log('reset');
            const [editor, navigator] = await getNavigator();
            ColorPicker.reset(editor);
            editor.setDecorations(AstHighlightDecorationType, []);
        })
    );
    context.subscriptions.push(vscode.commands.registerCommand('extension.goToParent', () => goToNode('up')));
    context.subscriptions.push(vscode.commands.registerCommand('extension.goToFirstChild', () => goToNode('down')));
    context.subscriptions.push(vscode.commands.registerCommand('extension.goToLeftSibling', () => goToNode('left')));
    context.subscriptions.push(vscode.commands.registerCommand('extension.goToRightSibling', () => goToNode('right')));

    context.subscriptions.push(
        vscode.commands.registerCommand('extension.useDefPath', async () => {
            const [editor, navigator] = await getNavigator();

            const paths = { sourceFile: editor.document.uri.fsPath, specificationFile: getSpecification() };
            const usages = await getUsages({
                paths,
                nodeId: navigator.currentNode.id
            });
            const result = await vscode.window.showQuickPick(
                usages.map(u => `${u.id}: ${u.name}: ${u.stackTrace[0]}`),
                {
                    placeHolder: 'choose usage'
                }
            );
            const id = (result && +result.split(':')[0]) as number;
            const path = await getUseDefPath({ paths, usageId: id });
            const keys = path.map((x, i) => i + ': ' + x.name);
            editor.setDecorations(AstHighlightDecorationType, []);
            await vscode.window.showQuickPick(keys, {
                placeHolder: 'Choose segment',
                onDidSelectItem: (item: string) => displaySegment(editor, path[+item.split(':')[0]].nodes)
            });
        })
    );

    vscode.workspace.onDidOpenTextDocument(doc => {
        const editor = vscode.window.activeTextEditor;
        if (editor !== undefined && !navigators.has(editor)) {
            console.log(editor.document.uri);

            getAst(getPaths(doc)).then(data => {
                navigators.set(editor, new AstNavigator(data));
            });
        }
    });

    vscode.workspace.onDidChangeTextDocument(doc => {
        notifyChange(getPaths(doc.document));
    });

    let disposable = vscode.commands.registerCommand('extension.display', () => {
        // The code you place here will be executed every time your command is executed
        const current = vscode.window.activeTextEditor;
        if (!current) {
            vscode.window.showErrorMessage('No file selected');
            return;
        }

        panel = vscode.window.createWebviewPanel(
            'renameDebugger', // Identifies the type of the webview. Used internally
            'AST View', // Title of the panel displayed to the user
            vscode.ViewColumn.Two, // Editor column to show the new webview panel in.
            { enableScripts: true } // Webview options. More on these later.
        );

        console.log(current.document.uri.fsPath);
        panel.webview.onDidReceiveMessage(
            async message => {
                const [editor, _] = await getNavigator();
                switch (message.command) {
                    case 'ready':
                        getAst(getPaths(editor.document)).then(data => {
                            // tslint:disable-next-line: no-unused-expression
                            panel && panel.webview.postMessage({ command: 'display', data: data });
                        });
                        return;
                    case 'selected':
                        console.log(message.data);
                        updateTreeViews({ paths: getPaths(editor.document), nodeId: message.data.id });
                }
            },
            undefined,
            context.subscriptions
        );
        panel.webview.html = getWebviewContent(context.extensionPath);
    });
    context.subscriptions.push(disposable);
}

function getWebviewContent(extensionRoot: string) {
    const scriptPath = path.join(extensionRoot, 'view.js');
    const stylePath = path.join(extensionRoot, 'view.css');
    const libPath = path.join(extensionRoot, 'adaptive-graph.js');
    const astViewURI = {
        script: vscode.Uri.file(scriptPath).with({ scheme: 'vscode-resource' }),
        style: vscode.Uri.file(stylePath).with({ scheme: 'vscode-resource' }),
        lib: vscode.Uri.file(libPath).with({ scheme: 'vscode-resource' }),
        d3: vscode.Uri.file(path.join(extensionRoot, 'd3.js')).with({ scheme: 'vscode-resource' }),
        flextree: vscode.Uri.file(path.join(extensionRoot, 'd3-flextree.min.js')).with({ scheme: 'vscode-resource' })
    };

    return `<!doctype html>
	<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Dagre Interactive Demo</title>
		
        <script src="${astViewURI.d3}"></script>
        <script src="${astViewURI.flextree}"></script>
        <script src="https://dagrejs.github.io/project/graphlib/v2.1.7/graphlib.js"></script>
        <script src="https://dagrejs.github.io/project/dagre/latest/dagre.js"></script>
		<script src="https://dagrejs.github.io/project/dagre-d3/v0.6.3/dagre-d3.js"></script>

		<link rel="stylesheet" type="text/css" href=${astViewURI.style}>
	</head>
	<body>
		<svg width=100% height=100%>
		<g/>
        </svg>
		<script src=${astViewURI.script}></script>
	</body>
	</html>`;
}
// this method is called when your extension is deactivated
export function deactivate() {}
