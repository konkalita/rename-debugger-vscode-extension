import * as vscode from 'vscode';
import { Location } from '../common/location';

export class VariableNode extends vscode.TreeItem {
    constructor(public label: string, public children: VariableNode[], public sourceLocation: Location) {
        super(
            label,
            children.length > 0 ? vscode.TreeItemCollapsibleState.Collapsed : vscode.TreeItemCollapsibleState.None
        );
    }

    public get description(): string {
        return (
            this.sourceLocation &&
            `${this.sourceLocation.source.name}:${this.sourceLocation.startLine}:${this.sourceLocation.startCharacter}`
        );
    }
}

export class VariableTreeProvider implements vscode.TreeDataProvider<VariableNode> {
    private model: VariableNode;
    constructor(adjacent: VariableNode[]) {
        const parent = new VariableNode('parent', [adjacent[0]], adjacent[0].sourceLocation);
        const children = adjacent.slice(1).map((x, i) => new VariableNode('child' + i, [x], x.sourceLocation));
        this.model = new VariableNode('node', [parent].concat(children), parent.sourceLocation);
    }
    private _onDidChangeTreeData: vscode.EventEmitter<VariableNode | undefined> = new vscode.EventEmitter<
        VariableNode | undefined
    >();

    refresh(): void {
        this._onDidChangeTreeData.fire();
    }

    readonly onDidChangeTreeData: vscode.Event<VariableNode | undefined> = this._onDidChangeTreeData.event;
    getTreeItem(element: VariableNode): vscode.TreeItem {
        return element;
    }
    getChildren(element?: VariableNode | undefined): Thenable<VariableNode[]> {
        console.log('children', element);
        return Promise.resolve(element ? element.children : [this.model]);
    }
}
