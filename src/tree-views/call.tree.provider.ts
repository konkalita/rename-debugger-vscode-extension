import * as vscode from 'vscode';
import { TreeItem, TreeItemCollapsibleState } from 'vscode';
import { Location } from '../common/location';

class CallTreeNode extends TreeItem {
    public children: CallTreeNode[] = [];
}

export class ArgNode extends CallTreeNode {
    public children: CallTreeNode[];
    constructor(args: any) {
        super('args', TreeItemCollapsibleState.Expanded);
        this.children = args.map(
            (arg: any) => new CallTreeNode(!!arg.variable ? `${arg.variable}: ${arg.value}` : arg.value)
        );
    }
}

export class CallNode extends CallTreeNode {
    constructor(
        public label: string,
        public children: CallTreeNode[],
        public args: any[],
        public sourceLocation: Location
    ) {
        super(label, vscode.TreeItemCollapsibleState.Collapsed);
        this.children = [new ArgNode(args) as CallTreeNode].concat(this.children);
    }

    public get description(): string {
        return `${this.sourceLocation.source.name}:${this.sourceLocation.startLine}:${this.sourceLocation.startCharacter}`;
    }
}

export class EquationNode extends CallTreeNode {
    constructor(public label: string, public children: CallTreeNode[], public sourceLocation: Location) {
        super(label, vscode.TreeItemCollapsibleState.None);
    }

    public get description(): string {
        return `${this.sourceLocation.source.name}:${this.sourceLocation.startLine}:${this.sourceLocation.startCharacter}`;
    }
}

export class CallTreeProvider implements vscode.TreeDataProvider<CallTreeNode> {
    constructor(private model: CallTreeNode) {
        model.collapsibleState = TreeItemCollapsibleState.Expanded;
    }
    private _onDidChangeTreeData: vscode.EventEmitter<CallTreeNode | undefined> = new vscode.EventEmitter<
        CallTreeNode | undefined
    >();

    refresh(): void {
        this._onDidChangeTreeData.fire();
    }

    readonly onDidChangeTreeData: vscode.Event<CallTreeNode | undefined> = this._onDidChangeTreeData.event;
    getTreeItem(element: CallTreeNode): vscode.TreeItem {
        return element;
    }
    getChildren(element?: CallTreeNode | undefined): Thenable<CallTreeNode[]> {
        console.log('children', element);
        return Promise.resolve(element ? element.children : [this.model]);
    }
}
