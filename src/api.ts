import * as rp from 'request-promise';
import * as vscode from 'vscode';

import { VariableNode } from './tree-views/variable.tree.provider';
import { CallNode, EquationNode } from './tree-views/call.tree.provider';
import * as path from 'path';
import { AstNode, SourceLocation } from './ast/ast.node';
import { Location } from './common/location';

export async function getAst(args: Paths): Promise<AstNode> {
    return request('ast_tree', args);
}

export async function notifyChange(args: Paths): Promise<void> {
    request('notify_change', args);
}

export async function getCallHistory(args: Paths): Promise<CallHistoryItem[]> {
    return request('rules_call_history', args) as Promise<CallHistoryItem[]>;
}

export async function getCallTree(args: { paths: Paths; nodeId: number }): Promise<CallNode | EquationNode> {
    return request('evaluated_rule', args).then(data => toCallNode(data));
}

export async function getEquations(args: { paths: Paths; nodeId: number }): Promise<VariableNode[]> {
    return request('evaluated_equations', args).then(data => data.map((x: any) => toVariableNode(x)));
}

export async function getUsages(args: { paths: Paths; nodeId: number }): Promise<Usage[]> {
    return request('usage', args);
}

export async function getDefinitions(args: { paths: Paths; nodeId: number }): Promise<Definition[]> {
    return request('definition', args);
}

export async function getUseDefPath(args: { paths: Paths; usageId: number }): Promise<PathSegment[]> {
    return request('definition_path', args);
}

export async function getVisibilityScope(args: { paths: Paths; definitionId: number }): Promise<Scope> {
    return request('visibility_scope', args);
}

async function request(command: string, data: any) {
    return rp({
        method: 'POST',
        uri: 'https://localhost:5001/api/' + command,
        insecure: true,
        body: data,
        rejectUnauthorized: false,
        json: true
    } as any).catch(err => console.log(command, err));
}

export interface Rule {
    name: string;
    type: string;
    sourceLocation: SourceLocation;
}
export interface CallHistoryItem {
    rule: Rule;
    calls: AstNode[];
}

export interface Usage {
    id: number;
    name: string;
    stackTrace: SourceLocation[];
}

export type Definition = Usage;

export interface PathSegment {
    name: string;
    nodes: AstNode[];
}

export interface Paths {
    sourceFile: string;
    specificationFile: string;
}

export type Scope = {
    [K in string]: AstNode[];
};

function toVariableNode(node: any) {
    if (!node) {
        return new VariableNode('', [], {
            source: path.parse(''),
            startCharacter: 0,
            startLine: 0,
            endCharacter: 0,
            endLine: 0
        });
    }
    if (node.sourceLocation) {
        node.sourceLocation.source = path.parse(node.sourceLocation.source);
    }

    const children = node.children.map(toVariableNode);
    return new VariableNode(node.label, children, node.sourceLocation);
}

function toCallNode(node: any) {
    if (node.key.sourceLocation) {
        node.key.sourceLocation.source = path.parse(node.key.sourceLocation.source);
    }
    const children = node.children.map(toCallNode);
    if (node.key.type === 'call') {
        return new CallNode(node.key.label, children, node.key.args, node.key.sourceLocation);
    }
    return new EquationNode(node.key.label, children, node.key.sourceLocation);
}
