import * as vscode from 'vscode';

export interface SourceLocation {
    source: string;
    startCharacter: number;
    endCharacter: number;
    startLine: number;
    endLine: number;
}
export interface AstNode {
    id: number;
    shortName: string;
    fullName: string;
    uses: any[];
    defs: any[];
    sourceLocation: SourceLocation;
    children: AstNode[];
}

export function toRange(location: SourceLocation) {
    return new vscode.Range(
        location.startLine - 1,
        location.startCharacter - 1,
        location.endLine - 1,
        location.endCharacter - 1
    );
}
