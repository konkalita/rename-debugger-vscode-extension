import * as vscode from 'vscode';
import { SourceLocation, AstNode, toRange } from './ast.node';

export class AstNavigator {
    private path: AstNode[];
    constructor(public currentNode: AstNode) {
        this.path = [this.currentNode];
    }

    public getIdFromRange(range: vscode.Range | vscode.Position): [number | null, AstNode[]] {
        const path: AstNode[] = [];
        const traverseTree = (node: AstNode): AstNode | null => {
            const nodeRange = toRange(node.sourceLocation);
            if (!nodeRange.contains(range)) {
                return null;
            }
            path.push(node);
            for (const child of node.children) {
                const min = traverseTree(child);
                if (min !== null) {
                    return min;
                }
            }
            return node;
        };
        const node = traverseTree(this.path[0]);
        return [node && node.id, path];
    }

    public goToPosition(position: vscode.Position) {
        const [id, path] = this.getIdFromRange(position);
        if (id !== null) {
            this.path = path;
            this.currentNode = this.path[this.path.length - 1];
            return this.currentNode;
        }
        return null;
    }

    public goTo(direction: 'up' | 'down' | 'left' | 'right'): AstNode | null {
        if (direction === 'up') {
            return this.goToParent();
        }
        if (direction === 'down') {
            return this.goToFirstChild();
        }
        if (direction === 'left') {
            return this.goToLeftSibling();
        }
        if (direction === 'right') {
            return this.goToRightSibling();
        }
        return null;
    }

    private goToParent(): AstNode | null {
        if (this.path.length > 1) {
            this.path.pop();
            this.currentNode = this.path[this.path.length - 1];

            return this.currentNode;
        }
        return null;
    }

    private goToFirstChild(): AstNode | null {
        if (this.currentNode.children.length > 0) {
            console.log(this.currentNode);
            this.currentNode = this.currentNode.children[0];
            this.path.push(this.currentNode);
            return this.currentNode;
        }
        return null;
    }

    private goToLeftSibling(): AstNode | null {
        if (this.path.length > 1) {
            const parent = this.path[this.path.length - 2];
            const currentIndex = parent.children.indexOf(this.currentNode);
            if (currentIndex > 0) {
                this.currentNode = parent.children[currentIndex - 1];
                this.path[this.path.length - 1] = this.currentNode;
                return this.currentNode;
            }
        }
        return null;
    }

    private goToRightSibling(): AstNode | null {
        if (this.path.length > 1) {
            const parent = this.path[this.path.length - 2];
            const currentIndex = parent.children.indexOf(this.currentNode);
            if (currentIndex + 1 < parent.children.length) {
                this.currentNode = parent.children[currentIndex + 1];
                this.path[this.path.length - 1] = this.currentNode;
                return this.currentNode;
            }
        }
        return null;
    }
}
