import * as vscode from 'vscode';
import { CodeLens, CodeLensProvider } from 'vscode';
import * as rp from 'request-promise';
import { SourceLocation, AstNode, toRange } from './ast/ast.node';
import { getPaths } from './common/utils';
import { getCallHistory } from './api';

export class RuleCodeLensProvider implements CodeLensProvider {
    async provideCodeLenses(document: vscode.TextDocument): Promise<CodeLens[]> {
        const paths = getPaths(document);
        const data = await getCallHistory(paths);
        return data.map(item => {
            const line = item.rule.sourceLocation.startLine - 1;
            let range = new vscode.Range(line, 1, line, 1);
            const locations = item.calls.map(
                call => new vscode.Location(vscode.Uri.file(call.sourceLocation.source), toRange(call.sourceLocation))
            );

            let c: vscode.Command = {
                command: 'editor.action.showReferences',
                title: `Executed ${item.calls.length} ${item.calls.length !== 1 ? 'times' : 'time'}`,
                arguments: [document.uri, range.start, locations]
            };

            return new CodeLens(range, c);
        });
    }
}
