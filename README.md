# renamedebugger README

## Requirements

VS Code >= 1.38.0

## Features:

-   ast navigation with `ctrl + [ijkl]`
-   select ast node corresponding to selection with `ctrl+u`
-   reset overlay with `ctrl+alt+p`
-   rule caltree preview
-   variables preview
-   usage-definition path
-   visibility scopes
