const vscode = acquireVsCodeApi();
const PATTERNS = [];

function convertToGraphlibFormat(data, boxPatterns) {
    function contractPaths(id) {
        data[id].children.forEach(contractPaths);
        if (data[id].children.length == 1 && data[data[id].children[0]].children.length == 0) {
            data[id].name = data[data[id].children[0]].name;
            data[id].children = [];
        }
    }
    const g = new adaptiveGraph.default.Graph({ directed: true, compound: true });
    // Set an object for the graph label
    g.setGraph({ nodesep: 10 });
    // Default to assigning a new object as a label for each new edge.
    g.setDefaultEdgeLabel(function() {
        return {};
    });

    data.forEach(node => {
        g.setNode('' + node.id, {
            label: node.shortName,
            height: 25,
            width: node.shortName.length * 8 + 10,
            sourceLocation: node.sourceLocation
        });
        for (let childId of node.children) {
            g.setEdge('' + node.id, '' + childId);
        }
    });

    // addBoxesAccordingToPatterns(g, data, boxPatterns);

    // contractPaths(0);

    console.log(g);
    return g;
}

function addBoxesAccordingToPatterns(g, data, patterns) {
    function addChildren(list, node) {
        node.children.forEach(child => {
            list.push(child);
            addChildren(list, data[+child]);
        });
    }

    const boxes = [];
    let counter = 0;

    g.nodes().forEach(id => {
        if (patterns.indexOf(g.node(id).label) >= 0) {
            const box = { id: 'b' + counter++, name: 'box', children: [id] };
            g.setNode('' + box.id, { label: box.name });
            console.log(g.inEdges(id));
            const e = g.inEdges(id)[0];
            console.log(e.v, box.id);
            // g.setEdge(e.v, box.id);
            // g.removeEdge(e);
            boxes.push(box);
            addChildren(box.children, data[id]);
            for (const childId of box.children) {
                g.setParent('' + childId, '' + box.id);
            }
            g.node(box.id).group = true;
        }
    });
}

function updateGraph(dotgraph) {
    if (oldInputGraphValue !== dotgraph) {
        oldInputGraphValue = dotgraph;
        try {
            g = convertToGraphlibFormat(dotgraph, PATTERNS);
        } catch (e) {
            throw e;
        }
        // Set margins, if not present
        if (!g.graph().hasOwnProperty('marginx') && !g.graph().hasOwnProperty('marginy')) {
            g.graph().marginx = 20;
            g.graph().marginy = 20;
        }
        g.graph().transition = function(selection) {
            return selection.transition().duration(500);
        };
        // Render the graph into svg g
        // d3.select('svg g').call(render, g);

        new Promise(() =>
            new adaptiveGraph.default.GraphRenderer(g, {
                nodeRenderer: adaptiveGraph.default.handlers.nodeRenderer,
                onClick: (id, renderer, g) => {
                    const n = g.node(id);
                    vscode.postMessage({
                        command: 'select',
                        data: {
                            start: n.startLocation,
                            end: n.endLocation
                        }
                    });
                },
                onMouseEnter: adaptiveGraph.default.handlers.hover(true),
                onMouseLeave: adaptiveGraph.default.handlers.hover(false)
            }).render()
        );
    }
}

function activate() {
    vscode.postMessage({ command: 'ready' });
}

window.addEventListener('message', event => {
    const message = event.data;
    switch (message.command) {
        case 'display':
            console.log('display');
            updateGraph(message.data);
            break;
    }
});

var oldInputGraphValue;

// Set up zoom support
var svg = d3.select('svg'),
    inner = d3.select('svg g'),
    zoom = d3.zoom().on('zoom', function() {
        inner.attr('transform', d3.event.transform);
    });
svg.call(zoom);
// Create and configure the renderer

function transform(x, y, w, h) {
    return [x - w / 2, y - h / 2];
}

activate();
